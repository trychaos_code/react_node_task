const express = require("express"),
  path = require("path"),
  fs = require("fs"),
  app = express();
import {createMiddleware} from "./middleware/createMiddleware"
import {createRoutes} from "./routes/createRoutes";
import {config} from "./config/configs";

const publicPath = path.resolve(__dirname, "../../client/dist");

createMiddleware(express, app, publicPath);
createRoutes(app, publicPath);

var server = app.listen(config.port, function() {
  console.log("listening on port : ", config.port);
  console.log("publicPath: ", publicPath);
});
