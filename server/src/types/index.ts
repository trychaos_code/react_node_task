interface Entity {
  name: string
}

export interface ICar {
  mileage: {
    number: number
    unit: string
  };
  stockNumber: number
  manufacturerName: string
  color: string
  modelName: string
  fuelType: string
  pictureUrl: string
}

export interface IManufacturer {
  models: Entity []
  name: string
}
