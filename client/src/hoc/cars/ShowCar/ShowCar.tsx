import React, { PureComponent } from 'react';
import { ICar } from '../../../types';
import { compare } from '../../../utils';
import { Button, Card, Img } from '../../../design';

export interface Props {
  details: ICar
  onClose: () => void
}
export const localStorageKey = 'saved_cars';

const dummyDescription = `This car is currently available and can be delivered as soon as
tomorrow morning. Please be aware that delivery times shown in
this page are not definitive and may change due to bad weather
conditions.`;
const actionText = `If you like this car, click the button and
save it in your collection of favourite
items.`;

const isDetailsSaved = (currentObj: ICar) => {
  let prevDetails = JSON.parse(localStorage.getItem(localStorageKey) || "[]");
  let matchedIndex = -1;
  prevDetails.forEach((details:ICar, index:number) => {
    if(compare(details, currentObj)) {
      matchedIndex = index;
    }
  });
  return matchedIndex;
};
export class ShowCar extends PureComponent<Props> {
  _isMounted = false;
  state = {
    matchedIndex: isDetailsSaved(this.props.details)
  };
  render() {
    const { details } = this.props;
    return (
      <div className="car-details">
        <div className="car-details-body">
          <Button className="bi-clear" onClick={this.onClose}>
            Close
          </Button>
          <div className="image-holder">
            <Img src={details.pictureUrl} alt={details.modelName}/>
          </div>
          <div className="pane">
            <div className="l-pane">
              <Card
                title={details.modelName}
                body={`Stock #${details.stockNumber} - ${details.mileage.number}${details.mileage.unit} - ${details.fuelType} - ${details.color}`}
                footer={dummyDescription}
                isBorderLess={true}
              />
            </div>
            <div className="r-pane">
              <Card body={actionText} footer={<Button onClick={() =>this.onSave()}>
                {this.state.matchedIndex !== -1 ? 'Remove' : 'Save'}
              </Button>}/>
            </div>
          </div>
        </div>
      </div>
    );
  }

  onClose = () => {
    this.props.onClose();
  };

  onSave = () => {
    const {details} = this.props;
    let {matchedIndex} = this.state;
    let prevDetails:Array<ICar> = JSON.parse(localStorage.getItem(localStorageKey) || "[]");
    if (matchedIndex === -1) {
      prevDetails.push(details);
      matchedIndex = prevDetails.length;
    } else {
      prevDetails.splice(matchedIndex, 1);
      matchedIndex = -1;
    }
    localStorage.setItem(localStorageKey, JSON.stringify(prevDetails));
    this._isMounted && this.setState({matchedIndex});
  };
  componentWillUnmount() {
    this._isMounted = false;
  }
  componentDidMount(): void {
    this._isMounted = true;
  }
}

