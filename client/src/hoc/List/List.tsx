import React, { Component } from 'react';
import cns from 'classnames';
import { Card, Select, Pagination } from '../../design';
import { Car, CarDetails } from '../cars';
import { filter, ICar } from '../../types';

export interface Props {
  listTitle?: string
  filters?: Array<filter>
  data?: CarDetails
}

interface stateI {
  sortOrder: string,
  cars?: Array<ICar>
}

const sortOption = [
  { uuid: '', label: 'None' },
  { uuid: 'milage_-1', label: 'Mileage - Ascending' },
  { uuid: 'milage_1', label: 'Mileage - Descending' }
];
const defaultData = { totalCarsCount: 0, totalPageCount: 1, cars: [] };

export class List extends Component<Props> {
  _isMounted = false;
  state: stateI = {
    sortOrder: sortOption[0].uuid,
    cars: (this.props.data || defaultData).cars
  };

  static getDerivedStateFromProps(props: Props, state: stateI) {
    const { filters, data } = props,
      { sortOrder } = state;
    let cars: Array<ICar> = data && data.cars ? JSON.parse(JSON.stringify(data.cars)) : [];
    sortOrder && cars.sort((a: ICar, b: ICar) => {
      const { mileage: { number: an } } = a;
      const { mileage: { number: bn } } = b;
      return sortOrder === 'milage_1'
        ? (an - bn)
        : (bn - an);
    });
    filters && filters.forEach(({ name, value }) => {
      if (typeof value !== 'string') {
        const { name: v_name, uuid, manufacturerName } = value;
        if (name === 'manufacturers' && manufacturerName) {
          cars = cars.filter(car => (
            (car.manufacturerName === manufacturerName) &&
            (car.modelName = name)
          ));
        } else if (name === 'color' && uuid) {
          cars = cars.filter(car => car.color === uuid);
        }
      }
    });
    return { sortOrder, cars };
  }

  render() {
    const { listTitle, data = defaultData } = this.props;
    const { cars = [] } = this.state;
    const { totalCarsCount, totalPageCount } = data;
    return (
      <div className="list-group">
        <div className={cns('header', 'list-header')}>
          <Card title={listTitle}
                body={`Showing ${Math.floor(totalCarsCount / totalPageCount)} of ${totalCarsCount} result`}
                isBorderLess={true}/>
          <Card title="Sort by"
                body={<Select name="sort_by" onChange={this.sortBy} options={sortOption}/>}
                isBorderLess={true}/>
        </div>
        <div className="list-group-items">
          {
            cars.length
              ? cars.map((details: ICar, ind: string | number | undefined) => <Car key={ind} details={details}/>)
              : <div className="no-result">No data found</div>
          }
          <Pagination totalPage={totalPageCount} onPageChange={this.onPageChange} />
        </div>
      </div>
    );
  }
  componentWillUnmount() {
    this._isMounted = false;
  }
  componentDidMount(): void {
    this._isMounted = true;
  }
  sortBy = (name: string, val: {uuid: string}) => {
    this._isMounted && this.setState({ sortOrder: val.uuid });
  };

  onPageChange = (newPage: number) => {
    console.log("newPage", newPage);
  };
}
