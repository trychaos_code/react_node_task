import { cleanup, render } from '@testing-library/react';
import { List, Props } from './List';
import React, {PureComponent} from 'react';
import { filter, defaultICar, ICar } from '../../types';
import {defaultFilters} from "../../views/Dashboard/filterUitls"
import { CarDetails } from '../cars/Car';

const defaultProps = {
  listTitle: 'listTitle',
  filters: defaultFilters,
  data: {
    totalCarsCount: 1000,
    totalPageCount: 5000,
    cars: [defaultICar]
  }
};
describe('<List />', () => {
  afterEach(cleanup);
  function renderList(props: Partial<Props> = {}) {
    props = {...defaultProps, ...props};
    // @ts-ignore
    const renderedElm = render(<List {...props}>Test List</List>);
    const { asFragment } = renderedElm;
    expect(asFragment()).toMatchSnapshot();
    return renderedElm;
  }
  it("render default", () => {
    renderList(defaultProps);
  });
});
