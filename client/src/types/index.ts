interface Entity {
  name: string
}

export interface ICar {
  mileage: {
    number: number
    unit: string
  };
  stockNumber: number
  manufacturerName: string
  color: string
  modelName: string
  fuelType: string
  pictureUrl: string
}
export const defaultICar =  {
  mileage: {
    number: 1,
    unit: 'km'
  },
  stockNumber: 1,
  manufacturerName: 'Audi',
  color: 'red',
  modelName: 'A4',
  fuelType: 'Petrol',
  pictureUrl: ''
};

export interface IManufacturer {
  models: Entity []
  name: string
}

export interface filter {
  value: string | {manufacturerName: string, name: string, uuid: string},
  formatter: Function,
  url: string,
  name: string,
  label: string,
  loadingText: string
}

export type manufacturer = {
  manufacturers: Array<{
    name: string,
    uuid:string,
    models: Array<{name:string, uuid:string}>
  }>
};

export type Ifilters = Array<{value: string | object, formatter: Function, url:string, name:string, label:string, loadingText: string}>
