import React from "react";
import { render, fireEvent, waitForElement, cleanup, findAllByText } from '@testing-library/react';

import {PageRoute, Props} from "./PageRoute";

describe("<PageRoute />", () => {
  afterEach(cleanup);
  function renderPageRoute(props: Partial<Props> = {}) {
    const defaultProps = {  };
    const renderedElm = render(<PageRoute {...defaultProps} {...props} />);

    const { asFragment } = renderedElm;
    expect(asFragment()).toMatchSnapshot();
    return renderedElm;
  }
  it("renders default", () => {
    renderPageRoute();
  });
  const findItemByText =  (text: string) => {
    return async () => {
      const { findByText } = renderPageRoute();
      const navItem = await findByText(text);
      fireEvent.click(navItem);
      const container = document.body;
      const textNodes = await waitForElement(() => findAllByText(container, text));
      (await textNodes).forEach(textNode => {
        expect(textNode).toBeVisible();
      })
    }
  };


  const navs = ["Random", "Dashboard", "Purchase", "My Orders", "Sell"];
  navs.forEach(nav => {
    it(`should render <Header /> and should display a default PageRoute with ${nav} in navs`, findItemByText(nav));
  });

  it('should render <Footer /> at the bottom', findItemByText('© ReactNode Group 2018'));

  it('should render <NavFilter />', () => {
    findItemByText('Loading colors ...');
    findItemByText('Loading manufacturers ...');
    findItemByText('Filter');
  });

});
