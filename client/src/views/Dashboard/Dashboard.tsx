import React, { Fragment, PureComponent, Component } from 'react';
import { NavFilter, List } from '../../hoc';
import { RequestLoader} from '../../design';

import { defaultFilters } from './filterUitls';
import { filter } from '../../types';

interface Props {
}
export class Dashboard extends Component<Props> {
  _isMounted = false;
  state = {
    carUrl: "/api/cars",
    filters: defaultFilters
  };

  render() {
    const {carUrl, filters} = this.state;
    return (<div className="page-container">
      <NavFilter onFilterChange={this.onFilterChange} filters={filters}/>
      <RequestLoader url={carUrl} loadingText="Fetching details">
        <List listTitle="Available cars" filters={filters}/>
      </RequestLoader>
    </div>);
  }
  componentWillUnmount() {
    this._isMounted = false;
  }
  componentDidMount(): void {
    this._isMounted = true;
  }
  onFilterChange = (filters:Array<filter>) => {
    this._isMounted && this.setState({
      filters
    })
  }
}
