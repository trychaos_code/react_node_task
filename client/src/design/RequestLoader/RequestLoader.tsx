import React, {Fragment, Children, cloneElement, isValidElement} from 'react';
import cns from 'classnames';
import {Request, RequestResponse, RequestError, Method} from '../../utils';

export interface Props {
    className?: string,
    method?: Method,
    url: string,
    loadingText?: string,
    param?: object
}
export class RequestLoader extends React.PureComponent<Props> {
    _isMounted = false;
    constructor(props: Props) {
        super(props);
    }
    state = {
        isLoading: true,
        fetchedData: RequestResponse,
        err: RequestError
    };
    render() {
        const {
            loadingText = 'Loading',
            className,
            children
        } = this.props;
        const {
            isLoading, fetchedData, err
        } = this.state;
        return (
          <Fragment>
              {
                  isLoading ? (
                    <div className={cns('loading', className)}>
                        {`${loadingText} ...`}
                    </div>
                  ) : (
                    err && err.code ? (
                      <div>Error with code: {err.code}</div>
                    ) : (
                      Children.map(children, child =>
                        isValidElement(child) ? cloneElement(child, fetchedData) : null
                      )
                    )
                  )
              }
          </Fragment>
        );
    }
    componentWillUnmount() {
        this._isMounted = false;
    }
    componentDidMount(): void {
        this._isMounted = true;
        const {
            method = 'get',
            url,
            param
        } = this.props;
        const req = new Request(url, method, param);
        req.then((res = RequestResponse, err = RequestError) => {
            this._isMounted && this.setState({
                fetchedData: res,
                err: err.code,
                isLoading: false
            })
        });
    }
}
