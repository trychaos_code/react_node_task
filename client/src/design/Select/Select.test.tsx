import { cleanup, render } from '@testing-library/react';
import { Select, Props, optionType, defaultOption } from './Select';
import React, {PureComponent} from 'react';

const defaultProps = {
  label: 'label',
  name: 'name',
  multi: true,
  data: [defaultOption],
  options: [defaultOption],
  selectedOption:defaultOption,
  onChange: () => {}
};
describe('<Select />', () => {
  afterEach(cleanup);
  function renderSelect(props: Partial<Props> = {}) {
    props = {...defaultProps, ...props};
    // @ts-ignore
    const renderedElm = render(<Select {...props} />);
    const { asFragment } = renderedElm;
    expect(asFragment()).toMatchSnapshot();
    return renderedElm;
  }
  it("render default", () => {
    renderSelect(defaultProps);
  });
});
