import { cleanup, render } from '@testing-library/react';
import { Logo, Props } from './Logo';
import React, {PureComponent} from 'react';

const defaultProps = {
  img: 'img'
};
describe('<Logo />', () => {
  afterEach(cleanup);
  function renderLogo(props: Partial<Props> = {}) {
    props = {...defaultProps, ...props};
    // @ts-ignore
    const renderedElm = render(<Logo {...props} />);
    const { asFragment } = renderedElm;
    expect(asFragment()).toMatchSnapshot();
    return renderedElm;
  }
  it("render default", () => {
    renderLogo(defaultProps);
  });
});
