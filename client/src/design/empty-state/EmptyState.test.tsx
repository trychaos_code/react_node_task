import { cleanup, render } from '@testing-library/react';
import { EmptyState, Props } from './EmptyState';
import React, {PureComponent} from 'react';

const defaultProps = {
  emptyIcon: 'emptyIcon',
  title: 'title',
  action: 'action',
  subtitle: 'subtitle'
};
describe('<EmptyState />', () => {
  afterEach(cleanup);
  function renderEmptyState(props: Partial<Props> = {}) {
    props = {...defaultProps, ...props};
    // @ts-ignore
    const renderedElm = render(<EmptyState {...props} />);
    const { asFragment } = renderedElm;
    expect(asFragment()).toMatchSnapshot();
    return renderedElm;
  }
  it("render default", () => {
    renderEmptyState(defaultProps);
  });
});
