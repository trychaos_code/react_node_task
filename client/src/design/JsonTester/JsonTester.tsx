import React, {Fragment} from 'react';
import cns from 'classnames';

export interface Props {
    data?: object
}
export class JsonTester extends React.PureComponent<Props> {
    render() {
        const {
            data = {}
        } = this.props;
        return (
          <Fragment>
              <pre>
                {JSON.stringify(data, null, 4)}
            </pre>
              <hr />
          </Fragment>
        );
    }
}
