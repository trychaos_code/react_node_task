import { cleanup, render } from '@testing-library/react';
import { JsonTester, Props } from './JsonTester';
import React, {PureComponent} from 'react';

const defaultProps = {
  a: 'a', b: [], c: 5, d: 'd'
};
describe('<JsonTester />', () => {
  afterEach(cleanup);
  function renderJsonTester(props?: { a: string; b: any[]; c: number; d: string }) {
    props = {...defaultProps, ...props};
    // @ts-ignore
    const renderedElm = render(<JsonTester {...props} />);
    const { asFragment } = renderedElm;
    expect(asFragment()).toMatchSnapshot();
    return renderedElm;
  }
  it("render default", () => {
    renderJsonTester(defaultProps);
  });
});
