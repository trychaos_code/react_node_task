import React, { FormEvent } from 'react';
import cns from 'classnames';

export interface Props {
  isSmall?: boolean,
  isPrimary?: boolean,
  success?: boolean,
  info?: boolean,
  failure?: boolean,
  isGreen?: boolean,
  isActive?: boolean,
  busy?: boolean,
  warning?: boolean,
  isText?: boolean,
  className?: string,
  disabled?: boolean,
  onClick?: () => void
}

export class Button extends React.PureComponent<Props> {
  render() {
    const {
      isSmall, isPrimary, success, info, failure, isGreen, isActive, busy, warning,
      isText, className, children, disabled
    } = this.props;
    const classes = {
      'btn': true,
      'btn-sm': isSmall,
      'btn-primary': isPrimary,
      'btn-success': success,
      'btn-info': info,
      'btn-danger': failure,
      'btn -action': isGreen,
      'active': isActive,
      'btn-busy': busy,
      'btn-warning': warning,
      'btn-link': isText,
      'btn-default':
        !isPrimary &&
        !warning &&
        !isText &&
        !failure
    };
    return (
      <button
        type="button"
        className={cns(classes, className)}
        onClick={this.onClick}
        disabled={disabled}
      >
        {children}
      </button>
    );
  }

  onClick = (ev: FormEvent) => {
    const { disabled, onClick } = this.props;
    if (!disabled) {
      ev.stopPropagation();
      onClick && onClick();
    }
  };
}

export default Button;
