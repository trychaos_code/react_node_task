import React, {cloneElement, PureComponent, Children, isValidElement} from 'react';
import cns from 'classnames';

export interface Props {
    className?: string
}
export class Tab extends PureComponent<Props> {
    render() {
        const {
            className,
            children
        } = this.props;
        return (
          <div className={cns('tab-list', className)}>
              {children}
          </div>
        );
    }
}
