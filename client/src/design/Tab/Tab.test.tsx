import { cleanup, render } from '@testing-library/react';
import { Tab, Props } from './Tab';
import React, {PureComponent} from 'react';

const defaultProps = {

};
describe('<Tab />', () => {
  afterEach(cleanup);
  function renderTab(props: Partial<Props> = {}) {
    props = {...defaultProps, ...props};
    // @ts-ignore
    const renderedElm = render(<Tab {...props}>Test Tab</Tab>);
    const { asFragment } = renderedElm;
    expect(asFragment()).toMatchSnapshot();
    return renderedElm;
  }
  it("render default", () => {
    renderTab(defaultProps);
  });
});
