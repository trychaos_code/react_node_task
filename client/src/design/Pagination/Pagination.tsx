import React, {ReactElement, PureComponent} from 'react';
import cns from 'classnames';
import {Button} from '../Button';

export interface Props {
  totalPage: number
  pageNumber?: number
  itemPerPage?: number
  onPageChange?: Function
}
export class Pagination extends PureComponent<Props> {
  _isMounted = false;
  state = {
    pageNumber: this.props.pageNumber || 0
  };
  render() {
    const { totalPage=0 } = this.props;
    const { pageNumber=0 } = this.state;
    return (
      <div className="pagination">
        <div className="pagination-container">
          <Button isText={true} disabled={pageNumber === 0} onClick={() => this.onPageChange(-1 * totalPage)}>First</Button>
          <Button isText={true} disabled={pageNumber === 0} onClick={() => this.onPageChange(-1)}>Previous</Button>
          <div className="btn">
            Page {pageNumber} of {totalPage}
          </div>
          <Button isText={true} disabled={pageNumber === totalPage} onClick={() => this.onPageChange(1)}>Next</Button>
          <Button isText={true} disabled={pageNumber === totalPage} onClick={() => this.onPageChange(totalPage)}>Last</Button>
        </div>
      </div>
    );
  }
  componentDidMount() {
    this._isMounted = true;
  }
  componentWillUnmount() {
    this._isMounted = false;
  }
  onPageChange = (noOfPage: number) => {
    const { totalPage, onPageChange } = this.props;
    const { pageNumber } = this.state;
    let newPage = (pageNumber + noOfPage) || 0;
    if(newPage > totalPage) {
      newPage = totalPage;
    } else if (newPage < 0) {
      newPage = 0;
    }
    this._isMounted && this.setState({pageNumber: newPage}, () => {
      onPageChange && onPageChange(newPage);
    });
  }
}
