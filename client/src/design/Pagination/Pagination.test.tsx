import { cleanup, findAllByText, render, waitForElement } from '@testing-library/react';
import { Pagination, Props } from './Pagination';
import React, {PureComponent} from 'react';

describe('<Pagination />', () => {
  afterEach(cleanup);
  function renderPagination(totalPage: number, pageNumber: number, itemPerPage: number) {
    const defaultProps = {
      totalPage,
      pageNumber,
      itemPerPage
    };
    const renderedElm = render(<Pagination {...defaultProps} />);
    const { asFragment } = renderedElm;
    expect(asFragment()).toMatchSnapshot();
    return renderedElm;
  }
  const textToFind = (container: HTMLElement, text: string) => {
    return async () => {
      const textNodes = await waitForElement(() => container.querySelectorAll('.btn'));
      let isNotFound = false;
      (await textNodes).forEach((textNode: any, index) => {
        expect(textNode).toBeVisible();
        console.log('textNode.innerText: ', text, index, textNode.innerText);
        if(~textNode.innerText.indexOf(text)) {
          isNotFound = true;
        }
      });
      expect(isNotFound).toBe(false);
    }
  };
  it("render default", () => {
    renderPagination(1000, 1, 10);
  });
  describe('on 2nd page', () => {

    let container = renderPagination(10, 2, 10).container;
    it('should render "First" page link', textToFind(container,  'First'));

    it('should render "Previous" page link', textToFind(container,  'Previous'));

    it('should render "Page 2 of 10"', textToFind(container,  'Page 2 of 10'));

    it('should render "Next" page link', textToFind(container,  'Next'));

    it('should render "Last" page link', textToFind(container,  'Last'));
  });

  describe('on first page', () => {
    let container = renderPagination(10, 0, 10).container;
    it('should disable "First" page link', textToFind(container, 'First'));

    it('should disable "Previous" page link', textToFind(container, 'Previous'));

    it('should enable "Next" page link', textToFind(container, 'Next'));
  });

  describe('on not last page and not first page', () => {
    let container = renderPagination(10, 4, 10).container;
    it('should enable "First" page link', textToFind(container, 'First'));

    it('should enable "Previous" page link',  textToFind(container, 'Previous'));

    it('should enable "Next" page link', textToFind(container, 'Next'));

    it('should enable "Last" page link',  textToFind(container, 'Last'));
  });

  describe('on last page', () => {
    let container = renderPagination(10, 10, 10).container;
    it('should disable "Next" page link',  textToFind(container, 'Next'));

    it('should disable "Last" page link', textToFind(container, 'Last'));
  });
});
