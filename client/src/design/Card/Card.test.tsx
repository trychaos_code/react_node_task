import { cleanup, render } from '@testing-library/react';
import { Card, Props } from './Card';
import React, {PureComponent} from 'react';

describe('<Card />', () => {
  afterEach(cleanup);
  function renderCard(props: Partial<Props> = {}) {
    const renderedElm = render(<Card {...props} />);
    const { asFragment } = renderedElm;
    expect(asFragment()).toMatchSnapshot();
    return renderedElm;
  }
  it("render default", () => {
    renderCard({
      title: 'title',
      body: 'body',
      footer: 'footer'
    });
  });
});
