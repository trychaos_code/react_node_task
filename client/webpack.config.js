const path = require('path');
const fs = require('fs');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const SWPrecacheWebpackPlugin = require('sw-precache-webpack-plugin');
const WebpackPwaManifest = require('webpack-pwa-manifest');
const { client_version: cv } = JSON.parse(fs.readFileSync(path.resolve(__dirname, '../config/build_configs.json')));

const PUBLIC_PATH = '/';
const tags = {
  css: ['./css/index.' + cv + '.css'],
  js: [
    './vendors.' + cv + '.js',
    './app.' + cv + '.js'
  ]
};

module.exports = {
  target: 'web',
  entry: {
    app: './src/index.tsx'
  },
  output: {
    filename: '[name].' + cv + '.js',
    path: path.resolve(__dirname, './dist'),
    chunkFilename: '[name].' + cv + '.js',
    publicPath: '/',
    libraryTarget: 'umd2'
  },
  devtool: 'eval-source-map',
  optimization: {
    splitChunks: {
      cacheGroups: {
        vendor: {
          test: /[\\/]node_modules[\\/]/,
          name: 'vendors',
          enforce: true,
          chunks: 'all'
        }
      }
    }
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: path.resolve(__dirname, './src/index.ejs'),
      baseHref: './',
      appMountId: 'app',
      minify: true,
      inject: false,
      templateParameters: function(compilation, assets, options) {
        return {
          title: 'ReactNode',
          tags,
          options: options,
          webpackConfig: compilation.options,
          webpack: compilation.getStats().toJson()
        };
      }
    }),
    new SWPrecacheWebpackPlugin(
      {
        cacheId: 'my-domain-cache-id',
        dontCacheBustUrlsMatching: /\.\w{8}\./,
        filename: 'service-worker.js',
        minify: true,
        navigateFallback: PUBLIC_PATH + 'index.html',
        staticFileGlobsIgnorePatterns: [/\.map$/, /manifest\.json$/]
      }
    ),
    new WebpackPwaManifest({
      name: 'ReactNode Web App',
      short_name: 'ReactNode',
      description: 'ReactNode Progressive Web App!',
      background_color: '#ffffff',
      theme_color: '#01579b',
      'theme-color': '#01579b',
      icons: [
        {
          src: path.resolve(__dirname, './public/images/logo.png'),
          sizes: [96, 128, 192, 256, 384, 512],
          destination: 'icons'
        },
        {
          src: path.resolve(__dirname, './public/images/logo.png'),
          size: '1024x1024',
          destination: 'icons'
        }
      ],
      start_url: '/'
    })
  ],
  resolve: {
    extensions: ['.ts', '.tsx', '.js', '.jsx']
  },
  module: {
    rules: [{ test: /\.tsx?$/, loader: 'ts-loader' }]
  }
};
